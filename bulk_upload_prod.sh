#!/bin/bash

HOST=$1
URL="/search/v1/file"
DIRECTORY=$2
echo $HOST$URL
for file in $DIRECTORY*; do
	echo $file
	while [ `jobs | wc -l` -ge 5 ] 
	do
		echo "sleeping"
		sleep 1
	done
	curl -F "file=@"$file";filename="$file $HOST$URL &
done
