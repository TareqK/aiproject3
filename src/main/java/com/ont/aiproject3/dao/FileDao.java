/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.dao;

import com.ont.aiproject3.entity.DataFile;
import com.ont.aiproject3.entity.IndexEntry;
import com.ont.aiproject3.entity.WordInvertedIndex;
import static com.ont.aiproject3.entity.WordInvertedIndex.POSITIONAL;
import static com.ont.aiproject3.entity.WordInvertedIndex.STEMMED;
import com.ont.aiproject3.search.KhojaStemmer;
import com.ont.aiproject3.utils.EntityManagerFactoryWrapper;
import static com.ont.aiproject3.utils.GlobalConstants.DIRECTORY_PATH;
import edu.stanford.nlp.international.arabic.process.ArabicTokenizer;
import edu.stanford.nlp.ling.CoreLabel;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

/**
 *
 * @author tareq
 */
public class FileDao {

    private static final Logger LOG = Logger.getLogger(FileDao.class.getName());
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(1);
    EntityManager em = EntityManagerFactoryWrapper.getEntityManager();

    public void addFile(InputStream fileInputStream, FormDataContentDisposition metadata) {
        EntityTransaction t = em.getTransaction();
        t.begin();
        DataFile dataFile = new DataFile(metadata.getFileName());
        int read = 0;
        try {

            em.persist(dataFile);
        } finally {
            t.commit();
        }
        byte[] bytes = new byte[1024];
        String fileName = DIRECTORY_PATH + "/" + dataFile.getFileId();
        File file = new File(fileName);
        try (OutputStream out = new FileOutputStream(file)) {
            while ((read = fileInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            EXECUTOR.submit(new IndexRunner(file, dataFile));
        } catch (IOException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }

    }

    public DataFile getDataFile(Integer dataFileId) {
        try {
            return em.createNamedQuery("DataFile.byId", DataFile.class)
                    .setParameter("file_id", dataFileId)
                    .getSingleResult();
        } catch (NoResultException ex) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    public List<DataFile> getFileList() {
        EntityTransaction t = em.getTransaction();
        t.begin();
        try {
            List<DataFile> resultList = em.createNamedQuery("DataFile.List", DataFile.class)
                    .getResultList();
            if (resultList.isEmpty()) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            } else {
                return resultList;
            }
        } finally {
            t.commit();
        }
    }

    protected static class IndexRunner implements Runnable {

        private final File file;
        private final DataFile dataFile;
        private static final Logger LOG = Logger.getLogger(IndexRunner.class.getName());
        private final EntityManager em;

        protected IndexRunner(File file, DataFile dataFile) {
            this.em = EntityManagerFactoryWrapper.getEntityManager();
            this.file = file;
            this.dataFile = dataFile;
        }

        @Override
        public void run() {
            try {

                String content = new String(Files.readAllBytes(Paths.get(file.toURI())));
                StringReader stringReader = new StringReader(content);
                ArabicTokenizer<CoreLabel> tokenizer = ArabicTokenizer.newArabicTokenizer(stringReader, new Properties());
                List<CoreLabel> tokens = tokenizer.tokenize();
                int wordCount = 1;
                for (CoreLabel coreLabel : tokens) {
                    coreLabel.setValue(coreLabel.value().replaceAll("\\p{P}", ""));
                    KhojaStemmer stemmer = new KhojaStemmer();
                    String toStem = new String(coreLabel.value().getBytes());
                    String stem = stemmer.stem(toStem);
                    EntityTransaction t = em.getTransaction();
                    t.begin();
                    try {
                        if (!coreLabel.value().isEmpty()) {
                            WordInvertedIndex positionalRie;
                            try {
                                positionalRie = em.createNamedQuery("WordInvertedIndex.positionalLastByWord", WordInvertedIndex.class)
                                        .setParameter("word", coreLabel.value())
                                        .getSingleResult();
                            } catch (NoResultException ex) {
                                positionalRie = new WordInvertedIndex(coreLabel.value(), POSITIONAL);
                                em.persist(positionalRie);
                            }
                            updateRie(positionalRie, dataFile, wordCount);
                            WordInvertedIndex stemmedRie;
                            if (stem != null) {
                                try {
                                    stemmedRie = em.createNamedQuery("WordInvertedIndex.stemmedlLastByWord", WordInvertedIndex.class)
                                            .setParameter("word", stem)
                                            .getSingleResult();
                                } catch (NoResultException ex) {
                                    stemmedRie = new WordInvertedIndex(stem, STEMMED);
                                    em.persist(stemmedRie);
                                }
                                updateRie(stemmedRie, dataFile, wordCount);
                            }
                            wordCount++;
                        }
                    } finally {
                        t.commit();
                    }
                }
                LOG.info("Completed Indexing File :".concat(dataFile.getName()));
            } catch (IOException ex) {
                LOG.severe(ex.getMessage());

            }
        }

        private void updateRie(WordInvertedIndex rie, DataFile file, Integer position) {
            rie.addEntry(file.getFileId(), position);
        }
    }

}
