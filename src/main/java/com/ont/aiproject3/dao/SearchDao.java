/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.dao;

import com.ont.aiproject3.entity.DataFile;
import com.ont.aiproject3.entity.IndexEntry;
import com.ont.aiproject3.entity.WordInvertedIndex;
import com.ont.aiproject3.entity.SearchQuery;
import com.ont.aiproject3.search.KhojaStemmer;
import com.ont.aiproject3.utils.EntityManagerFactoryWrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.ws.rs.WebApplicationException;

/**
 *
 * @author tareq
 */
public class SearchDao {

    EntityManager em = EntityManagerFactoryWrapper.getEntityManager();
    private static final ExecutorService EXECUTOR = Executors.newCachedThreadPool();

    public List<WordInvertedIndex> getIndexes() {
            List<WordInvertedIndex> resultList = em.createNamedQuery("WordInvertedIndex.List", WordInvertedIndex.class)
                    .setLockMode(LockModeType.NONE )
                     .setHint("javax.persistence.lock.timeout", "-2")
                    .getResultList();
            if (resultList.isEmpty()) {
                throw new WebApplicationException(404);
            } else {
                return resultList;
            }
    }

    public List<DataFile> search(SearchQuery searchQuery) {
        if (searchQuery.getQuery().contains("\"")) {
            return positionalSearch(searchQuery);
        } else {
            return stemmedSearch(searchQuery);
        }
    }

    private List<DataFile> stemmedSearch(SearchQuery searchQuery) {
        List<FutureTask<IndexEntry>> taskList = new ArrayList();
        KhojaStemmer stemmer = new KhojaStemmer();
        String[] split = searchQuery.getQuery().replaceAll("\\p{P}", "").split(" ");
        for (String string : split) {
            String stemmed = stemmer.stem(string);
            if (stemmed != null) {
                taskList.add((FutureTask<IndexEntry>) EXECUTOR.submit(new StemmedSearchCall(stemmed)));
            }
        }
        IndexEntry result = new IndexEntry();
        try {
            if (!taskList.isEmpty()) {
                result = taskList.get(0).get();
                taskList.remove(0);
                for (FutureTask<IndexEntry> task : taskList) {
                    result.getEntries().keySet().retainAll(task.get().getEntries().keySet());
                }
            }
        } catch (InterruptedException | ExecutionException ex) {

        }
        if (result.getEntries().isEmpty()) {
            throw new WebApplicationException(404);
        } else {
            return getFiles(result);
        }
    }

    private List<DataFile> positionalSearch(SearchQuery searchQuery) {
        String cleaned = searchQuery.getQuery().replaceAll("\\p{P}", "");
        String[] splits = cleaned.trim().split(" ");
        ArrayList<String> split = new ArrayList();
        split.addAll(Arrays.asList(splits));
        HashSet<String> result = new HashSet();
        WordInvertedIndex currentRie = null;
        for (String word : split) {
            WordInvertedIndex nextRie;
            try {
                List<WordInvertedIndex> rieList = em.createNamedQuery("WordInvertedIndex.positionalIndexByWord", WordInvertedIndex.class)
                        .setParameter("word", word)
                        .getResultList();
                nextRie = new WordInvertedIndex();
                for (WordInvertedIndex index : rieList) {
                    nextRie.getEntries().addAll(index.getEntries().getEntries());
                }
            } catch (NoResultException ex) {
                throw new WebApplicationException(404);
            }
            if (currentRie != null) {
                for (String file : currentRie.getEntries().getEntries().keySet()) {// for every found file containing this word
                    for (Integer wordPosition : currentRie.getEntries().getEntries().get(file)) { // for every instance of this word in the file
                        Integer desiredPosition = wordPosition + 1;//the position we want is the one next to it
                        if (nextRie.getEntries().getEntries().containsKey(file)) {//if the index of the next word contains the file
                            if (nextRie.getEntries().getEntries().get(file).contains(desiredPosition)) {//and if that file contains  the desired position
                                result.add(file);//add the file to the result
                                break;// and dot continue checking this file for this index, because its already been added.
                            }
                        }
                    }
                }
            } else {
                if (split.size() == 1) {
                    result.addAll(nextRie.getEntries().getEntries().keySet());
                }
            }
            currentRie = nextRie;
        }
        if (result.isEmpty()) {
            throw new WebApplicationException();
        } else {
            return getFiles(result);
        }
    }

    private List<DataFile> getFiles(IndexEntry result) {
        return getFiles(result.getEntries().keySet());
    }

    private List<DataFile> getFiles(Collection<String> entries) {
        FileDao dao = new FileDao();
        ArrayList<DataFile> foundFiles = new ArrayList();
        entries.forEach((fileId) -> {
            foundFiles.add(dao.getDataFile(Integer.parseInt(fileId)));
        });
        if (foundFiles.isEmpty()) {
            throw new WebApplicationException(404);
        } else {
            return foundFiles;
        }
    }

    protected static class StemmedSearchCall implements Callable<IndexEntry> {

        EntityManager em;
        String searchKeyword;

        public StemmedSearchCall(String searchKeyword) {
            this.em = EntityManagerFactoryWrapper.getEntityManager();
            this.searchKeyword = searchKeyword;
        }

        @Override
        public IndexEntry call() throws Exception {
            WordInvertedIndex resultRie = new WordInvertedIndex();
            List<WordInvertedIndex> rie = em.createNamedQuery("WordInvertedIndex.stemmedIndexByWord", WordInvertedIndex.class)
                    .setParameter("word", searchKeyword)
                    .getResultList();
           for (WordInvertedIndex index : rie) {
                resultRie.getEntries().addAll(index.getEntries().getEntries());
            }
            return resultRie.getEntries();
           
        }
    }
}
