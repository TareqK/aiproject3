/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.endpoint;

import com.ont.aiproject3.dao.FileDao;
import com.ont.aiproject3.entity.DataFile;
import static com.ont.aiproject3.utils.GlobalConstants.DIRECTORY_PATH;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.server.ManagedAsync;

/**
 *
 * @author tareq
 */
@Path("file")
public class FileEndpoint {
   
    @POST
    @Consumes(value = {MediaType.MULTIPART_FORM_DATA})
    @ManagedAsync
    public void uploadAttachement(@Suspended final AsyncResponse asyncResponse,
            @FormDataParam(value = "file") final InputStream fileInputStream,
            @FormDataParam(value = "file") final FormDataContentDisposition fileMetaData) {
        asyncResponse.resume(doUploadFile(fileInputStream, fileMetaData));
    }
    
    private Response doUploadFile(InputStream fileInputStream, FormDataContentDisposition fileMetaData) {
        new FileDao().addFile(fileInputStream, fileMetaData); 
        return Response.ok().build();
    }
     
    @GET
    @Path(value = "{file_id}")
    @Produces({MediaType.APPLICATION_OCTET_STREAM})
    @ManagedAsync
    public void downloadFile(@Suspended final AsyncResponse asyncResponse,
            @PathParam(value = "file_id") final Integer fileId) {
        asyncResponse.resume(doDownloadFile(fileId));
    }

    private Response doDownloadFile(Integer fileId) {
            DataFile dataFile = new FileDao().getDataFile(fileId);
            File diskFile = new File(DIRECTORY_PATH+"/"+dataFile.getFileId());
            return Response.ok(diskFile,MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition", "attachment; filename = " + dataFile.getName())
                    .build();
    }
    
}
