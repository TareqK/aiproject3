/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.endpoint;

import com.ont.aiproject3.dao.SearchDao;
import com.ont.aiproject3.entity.DataFile;
import com.ont.aiproject3.entity.WordInvertedIndex;
import com.ont.aiproject3.entity.SearchQuery;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.server.ManagedAsync;

/**
 *
 * @author tareq
 */
@Path("query")
public class SearchEndpoint {
    
    
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @ManagedAsync
    public void search(@Suspended final AsyncResponse asyncResponse, SearchQuery searchQuery){
        asyncResponse.resume(doSearch(searchQuery));
    }
    
    public List<DataFile> doSearch(SearchQuery searchQuery){
        return new SearchDao().search(searchQuery);
    }
}
