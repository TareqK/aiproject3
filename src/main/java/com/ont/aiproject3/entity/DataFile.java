/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;



/**
 *
 * @author tareq
 */
@Entity
@NamedQueries({
    @NamedQuery(name="DataFile.byId", query = "SELECT df from DataFile df where df.fileId=(:file_id)"),
    @NamedQuery(name="DataFile.List", query = "SELECT df from DataFile df")
})
public class DataFile implements Serializable {


    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)
    @JsonProperty
    private Integer fileId;

    @JsonIgnore
    private String name;

    @JsonProperty
    private String canonicalName;

    @JsonProperty
    private String description;

    @JsonProperty
    public Integer getFileId() {
        return fileId;
    }

    @JsonIgnore
    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    @JsonIgnore
    public String getName() {
        return name;
    }

    @JsonIgnore
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getCanonicalName() {
        return canonicalName;
    }

    @JsonIgnore
    public void setCanonicalName(String canonicalName) {
        this.canonicalName = canonicalName;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    @JsonProperty
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    public DataFile(String fileName){
        this.canonicalName = fileName;
        this.name = fileName;
        this.description = fileName;
    }
    
    public DataFile(){
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (int) (this.fileId ^ (this.fileId >>> 32));
        hash = 53 * hash + Objects.hashCode(this.canonicalName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataFile other = (DataFile) obj;
        if (this.fileId != other.fileId) {
            return false;
        }
        return true;
    }

}
