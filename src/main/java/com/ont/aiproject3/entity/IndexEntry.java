/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author tareq
 */
public class IndexEntry implements Serializable {

    private HashMap<String, List<Integer>> entry;

    public HashMap<String, List<Integer>> getEntries() {
        return entry;
    }

    public void setEntries(HashMap<String, List<Integer>> entry) {
        this.entry = entry;
    }

    public void addEntry(DataFile dataFile, Integer word) {
        addEntry(dataFile.getFileId(), word);
    }

    public void addEntry(Integer fileId, Integer word) {
        if (entry == null) {
            entry = new HashMap();
        }
        if (entry.get(String.valueOf(fileId)) == null) {
            entry.put(String.valueOf(fileId), new ArrayList<Integer>());
        }
        entry.get(String.valueOf(fileId)).add(word);
    }
    
    public void addAll(HashMap<String,List<Integer>> entries){
        entry.putAll(entries);
    }

    public IndexEntry() {
        this.entry = new HashMap();
        
    }
    
    
}
