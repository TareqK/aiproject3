/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.ClobType;
import org.hibernate.usertype.UserType;

/**
 *
 * @author tareq
 */
public class IndexEntryType implements UserType {

    @Override
    public int[] sqlTypes() {
        return new int[]{
            ClobType.INSTANCE.sqlType()
        };
    }

    @Override
    public Class returnedClass() {
        return IndexEntry.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return x.equals(y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet arg0, String[] arg1, SharedSessionContractImplementor arg2, Object arg3) throws HibernateException, SQLException {
        assert (arg1.length >= 1);
        String string = arg0.getString(arg1[0]);
        HashMap<String,List<Integer>> readValue;
        try {
            readValue = new ObjectMapper().readValue(string, HashMap.class);
        } catch (IOException ex) {
            throw new HibernateException(" Failed to read JSON");
        }
        IndexEntry i = new IndexEntry();
        i.setEntries(readValue);
        return i;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        IndexEntry indexEntry = (IndexEntry) value;
        HashMap<String, List<Integer>> entries = indexEntry.getEntries();
        try {
            String json = new ObjectMapper().writeValueAsString(entries);
            st.setString(index, json);
        } catch (JsonProcessingException ex) {
            throw new HibernateException("Could not convert to json");
        }

    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        IndexEntry indexEntry2 = (IndexEntry) value;
        IndexEntry toReturn = new IndexEntry();
        toReturn.setEntries(indexEntry2.getEntries());
        return toReturn;
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return deepCopy(target);
    }

}
