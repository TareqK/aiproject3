/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author tareq
 */
@NamedQueries({
    @NamedQuery(name = "WordInvertedIndex.positionalIndexByWord", query = "SELECT r from WordInvertedIndex r WHERE r.word=(:word) AND r.type=0"),
    @NamedQuery(name = "WordInvertedIndex.stemmedIndexByWord", query = "SELECT r from WordInvertedIndex r WHERE r.word=(:word) AND r.type=1"),
    @NamedQuery(name = "WordInvertedIndex.positionalLastByWord", query= "SELECT r from WordInvertedIndex r WHERE r.word=(:word) AND r.type = 0 AND r.count<10"),
    @NamedQuery(name = "WordInvertedIndex.stemmedlLastByWord", query= "SELECT r from WordInvertedIndex r WHERE r.word=(:word) AND r.type=1 AND r.count<10"),
    @NamedQuery(name = "WordInvertedIndex.List", query = "SELECT r from WordInvertedIndex r")

})
@Entity
@Table(indexes = { @Index(name="word_index",columnList="word")})
public class WordInvertedIndex implements Serializable {
    public static final Integer LIMIT = 10;
    public static final transient Integer POSITIONAL = 0;
    public static final transient Integer STEMMED = 1;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;



    @Column(name="word", columnDefinition = "varchar(30)")
    private String word;

    private Integer type;

    private Integer count;
    
    @Type(type = "com.ont.aiproject3.entity.IndexEntryType")
    @Column(name="entries")
    @Lob
    private IndexEntry entries;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public IndexEntry getEntries() {
        if (this.entries == null) {
            this.entries = new IndexEntry();
        }
        return entries;
    }

    public void setEntries(IndexEntry entries) {
        this.entries = entries;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public WordInvertedIndex(){
        this.entries = new IndexEntry();
        this.count = 0;
        
    }
    
     public WordInvertedIndex(String word, Integer type){
         this.word = word;
        this.entries = new IndexEntry();
        this.count = 0;
        this.type = type;
    }
     

    
    public void addEntry(Integer fileId,  Integer wordPosition){
        if(!this.entries.getEntries().containsKey(String.valueOf(fileId))){
            this.count ++;
        }
        this.entries.addEntry(fileId, wordPosition);
    }
}
