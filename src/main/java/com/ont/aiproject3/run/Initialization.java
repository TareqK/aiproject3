/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.run;

import com.ont.aiproject3.utils.EntityManagerFactoryWrapper;
import static com.ont.aiproject3.utils.GlobalConstants.DIRECTORY_PATH;
import java.io.File;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


/**
 *
 * @author tareq
 */
@WebListener
public class Initialization implements ServletContextListener {
static{
    System.setProperty("file.encoding","UTF-8") ;
    System.setProperty("derby.ui.codeset","UTF8");
}
    private static final Logger LOG = Logger.getLogger(Initialization.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        EntityManagerFactoryWrapper.create("com.ont.aiproject3_PU");
        File file = new File(DIRECTORY_PATH);
        file.mkdirs();
        LOG.info("Application Launched");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        EntityManagerFactoryWrapper.destroy();
        LOG.info("Application Shutdown");
    }

}
