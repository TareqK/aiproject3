package com.ont.aiproject3.search;

/*

Arabic Stemmer: This program stems Arabic words and returns their root.
Copyright (C) 2002 Shereen Khoja

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

Computing Department
Lancaster University
Lancaster
LA1 4YR
s.Khoja@lancaster.ac.uk


KhojaStemmer class
This is the class that does all the work
It takes in a file containg the text to
be stemmed. The text is then read one line
at a time so large amounts of text can be
efficiently processed.
The class also takes in the ArrayList
containing all the contents of the static
files.
The class now tests for stopwords correctly,
and removes punctuation and diacritics (though
I haven't decided whether to return them to the
word in the final document or not)


Last Modified: 11/6/2001
 */
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KhojaStemmer implements Stemmer<String> {

    private static ArrayList<String> readFile(String string) throws IOException {

        ArrayList array = new ArrayList();
        File file = new File(KhojaStemmer.class.getClassLoader().getResource("/StemmerFiles/" + string).getFile());
        if (file.exists()) {
            try (FileInputStream fis = new FileInputStream(file)) {
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                String readString = new String(data).trim().replaceAll(" +", " ");
                String splits[] = readString.split(" ");
                array.addAll(Arrays.asList(splits));
                return array;
            }
        } else {
            throw new IOException("File not found");
        }

    }
    //--------------------------------------------------------------------------

    // boolean variable to check the files
    protected boolean couldNotOpenFile = false;
    // the files containing prefixes, suffixes and so on
    // have the root, pattern, stopword or strange words been found
    private boolean rootFound = false;
    private boolean stopwordFound = false;
    private boolean rootNotFound = false;
    private boolean fromSuffixes = false;
    private final String[][] possibleRoots;
    private final ArrayList wordsNotStemmed = new ArrayList();
    private static final ArrayList<List<String>> STAT_FILES;

    static {
        STAT_FILES = new ArrayList();
        try {
            // create a string buffer containing the path to the static files
            String pathToStemmerFiles = "";
            STAT_FILES.add(readFile(pathToStemmerFiles + "definite_article.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "duplicate.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "first_waw.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "first_yah.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "last_alif.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "last_hamza.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "last_maksoura.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "last_yah.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "mid_waw.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "mid_yah.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "prefixes.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "punctuation.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "quad_roots.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "stopwords.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "suffixes.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "tri_patt.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "tri_roots.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "diacritics.txt"));
            STAT_FILES.add(readFile(pathToStemmerFiles + "strange.txt"));
            // the vector was successfully created
            System.out.println("read in files successfully");

        } catch (IOException ex) {
            Logger.getLogger(KhojaStemmer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    int number = 0;

    //--------------------------------------------------------------------------
    // constructor
    public KhojaStemmer() {
        possibleRoots = new String[10000][2];

    }

    //--------------------------------------------------------------------------
    // format the word by removing any punctuation, diacritics and non-letter charracters
    //--------------------------------------------------------------------------
    // stem the word
    private String stemWord(String word) {
        rootFound = false;
        stopwordFound = false;
        word = removeDiacritics(word);
        // check if the word consists of two letters
        // and find it's root
        if (word.length() == 2) {
            word = isTwoLetters(word);
        }

        // if the word consists of three letters
        if (word.length() == 3 && !rootFound) // check if it's a root
        {
            word = isThreeLetters(word);
        }

        // if the word consists of four letters
        if (word.length() == 4) // check if it's a root
        {
            isFourLetters(word);
        }

        // if the root hasn't yet been found
        if (!rootFound) {
            // check if the word is a pattern
            word = checkPatterns(word);
        }

        // if the root still hasn't been found
        if (!rootFound) {
            // check for a definite article, and remove it
            word = checkDefiniteArticle(word);
        }

        // if the root still hasn't been found
        if (!rootFound && !stopwordFound) {
            // check for the prefix waw
            word = checkPrefixWaw(word);
        }

        // if the root STILL hasnt' been found
        if (!rootFound && !stopwordFound) {
            // check for suffixes
            word = checkForSuffixes(word);
        }

        // if the root STILL hasn't been found
        if (!rootFound && !stopwordFound) {
            // check for prefixes
            word = checkForPrefixes(word);
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // check and remove any prefixes
    private String checkForPrefixes(String word) {
        String prefix = "";
        String modifiedWord = word;
        ArrayList prefixes = (ArrayList) STAT_FILES.get(10);

        // for every prefix in the list
        for (int i = 0; i < prefixes.size(); i++) {
            prefix = (String) prefixes.get(i);
            // if the prefix was found
            if (prefix.regionMatches(0, modifiedWord, 0, prefix.length())) {
                modifiedWord = modifiedWord.substring(prefix.length());

                // check to see if the word is a stopword
                if (checkStopwords(modifiedWord)) {
                    return modifiedWord;
                }

                // check to see if the word is a root of three or four letters
                // if the word has only two letters, test to see if one was removed
                if (modifiedWord.length() == 2) {
                    modifiedWord = isTwoLetters(modifiedWord);
                } else if (modifiedWord.length() == 3 && !rootFound) {
                    modifiedWord = isThreeLetters(modifiedWord);
                } else if (modifiedWord.length() == 4) {
                    isFourLetters(modifiedWord);
                }

                // if the root hasn't been found, check for patterns
                if (!rootFound && modifiedWord.length() > 2) {
                    modifiedWord = checkPatterns(modifiedWord);
                }

                // if the root STILL hasn't been found
                if (!rootFound && !stopwordFound && !fromSuffixes) {
                    // check for suffixes
                    modifiedWord = checkForSuffixes(modifiedWord);
                }

                if (stopwordFound) {
                    return modifiedWord;
                }

                // if the root was found, return the modified word
                if (rootFound && !stopwordFound) {
                    return modifiedWord;
                }
            }
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // METHOD CHECKFORSUFFIXES
    private String checkForSuffixes(String word) {
        String suffix = "";
        String modifiedWord = word;
        ArrayList suffixes = (ArrayList) STAT_FILES.get(14);
        fromSuffixes = true;

        // for every suffix in the list
        for (int i = 0; i < suffixes.size(); i++) {
            suffix = (String) suffixes.get(i);

            // if the suffix was found
            if (suffix.regionMatches(0, modifiedWord, modifiedWord.length() - suffix.length(), suffix.length())) {
                modifiedWord = modifiedWord.substring(0, modifiedWord.length() - suffix.length());

                // check to see if the word is a stopword
                if (checkStopwords(modifiedWord)) {
                    fromSuffixes = false;
                    return modifiedWord;
                }

                // check to see if the word is a root of three or four letters
                // if the word has only two letters, test to see if one was removed
                switch (modifiedWord.length()) {
                    case 2:
                        modifiedWord = isTwoLetters(modifiedWord);
                        break;
                    case 3:
                        modifiedWord = isThreeLetters(modifiedWord);
                        break;
                    case 4:
                        isFourLetters(modifiedWord);
                        break;
                    default:
                        break;
                }

                // if the root hasn't been found, check for patterns
                if (!rootFound && modifiedWord.length() > 2) {
                    modifiedWord = checkPatterns(modifiedWord);
                }

                if (stopwordFound) {
                    fromSuffixes = false;
                    return modifiedWord;
                }

                // if the root was found, return the modified word
                if (rootFound) {
                    fromSuffixes = false;
                    return modifiedWord;
                }
            }
        }
        fromSuffixes = false;
        return word;
    }

    //--------------------------------------------------------------------------
    // check and remove the special prefix (waw)
    private String checkPrefixWaw(String word) {
        String modifiedWord = "";

        if (word.length() > 3 && word.charAt(0) == 'و') {
            modifiedWord = word.substring(1);

            // check to see if the word is a stopword
            if (checkStopwords(modifiedWord)) {
                return modifiedWord;
            }

            // check to see if the word is a root of three or four letters
            // if the word has only two letters, test to see if one was removed
            if (modifiedWord.length() == 2) {
                modifiedWord = isTwoLetters(modifiedWord);
            } else if (modifiedWord.length() == 3 && !rootFound) {
                modifiedWord = isThreeLetters(modifiedWord);
            } else if (modifiedWord.length() == 4) {
                isFourLetters(modifiedWord);
            }

            // if the root hasn't been found, check for patterns
            if (!rootFound && modifiedWord.length() > 2) {
                modifiedWord = checkPatterns(modifiedWord);
            }

            // if the root STILL hasnt' been found
            if (!rootFound && !stopwordFound) {
                // check for suffixes
                modifiedWord = checkForSuffixes(modifiedWord);
            }

            // iIf the root STILL hasn't been found
            if (!rootFound && !stopwordFound) {
                // check for prefixes
                modifiedWord = checkForPrefixes(modifiedWord);
            }

            if (stopwordFound) {
                return modifiedWord;
            }

            if (rootFound && !stopwordFound) {
                return modifiedWord;
            }
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // check and remove the definite article
    private String checkDefiniteArticle(String word) {
        // looking through the ArrayList of definite articles
        // search through each definite article, and try and
        // find a match
        String definiteArticle = "";
        String modifiedWord = "";
        ArrayList definiteArticles = (ArrayList) STAT_FILES.get(0);

        // for every definite article in the list
        for (int i = 0; i < definiteArticles.size(); i++) {
            definiteArticle = (String) definiteArticles.get(i);
            // if the definite article was found
            if (definiteArticle.regionMatches(0, word, 0, definiteArticle.length())) {
                // remove the definite article
                modifiedWord = word.substring(definiteArticle.length(), word.length());

                // check to see if the word is a stopword
                if (checkStopwords(modifiedWord)) {
                    return modifiedWord;
                }

                // check to see if the word is a root of three or four letters
                // if the word has only two letters, test to see if one was removed
                if (modifiedWord.length() == 2) {
                    modifiedWord = isTwoLetters(modifiedWord);
                } else if (modifiedWord.length() == 3 && !rootFound) {
                    modifiedWord = isThreeLetters(modifiedWord);
                } else if (modifiedWord.length() == 4) {
                    isFourLetters(modifiedWord);
                }

                // if the root hasn't been found, check for patterns
                if (!rootFound && modifiedWord.length() > 2) {
                    modifiedWord = checkPatterns(modifiedWord);
                }

                // if the root STILL hasnt' been found
                if (!rootFound && !stopwordFound) {
                    // check for suffixes
                    modifiedWord = checkForSuffixes(modifiedWord);
                }

                // if the root STILL hasn't been found
                if (!rootFound && !stopwordFound) {
                    // check for prefixes
                    modifiedWord = checkForPrefixes(modifiedWord);
                }

                if (stopwordFound) {
                    return modifiedWord;
                }

                // if the root was found, return the modified word
                if (rootFound && !stopwordFound) {
                    return modifiedWord;
                }
            }
        }
        if (modifiedWord.length() > 3) {
            return modifiedWord;
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // if the word consists of two letters
    private String isTwoLetters(String word) {
        // if the word consists of two letters, then this could be either
        // - because it is a root consisting of two letters (though I can't think of any!)
        // - because a letter was deleted as it is duplicated or a weak middle or last letter.

        word = duplicate(word);

        // check if the last letter was weak
        if (!rootFound) {
            word = lastWeak(word);
        }

        // check if the first letter was weak
        if (!rootFound) {
            word = firstWeak(word);
        }

        // check if the middle letter was weak
        if (!rootFound) {
            word = middleWeak(word);
        }

        return word;
    }

    //--------------------------------------------------------------------------
    // if the word consists of three letters
    private String isThreeLetters(String word) {
        StringBuilder modifiedWord = new StringBuilder(word);
        String root = "";
        // if the first letter is a '�', '�'  or '�'
        // then change it to a '�'
        if (word.length() > 0) {
            if (word.charAt(0) == 'ا' || word.charAt(0) == 'ؤ' || word.charAt(0) == 'ئ') {
                modifiedWord.setLength(0);
                modifiedWord.append('أ');
                modifiedWord.append(word.substring(1));
                root = modifiedWord.toString();
            }

            // if the last letter is a weak letter or a hamza
            // then remove it and check for last weak letters
            if (word.charAt(2) == 'و' || word.charAt(2) == 'ي' || word.charAt(2) == 'ا'
                    || word.charAt(2) == 'ى' || word.charAt(2) == 'ء' || word.charAt(2) == 'ئ') {
                root = word.substring(0, 2);
                root = lastWeak(root);
                if (rootFound) {
                    return root;
                }
            }

            // if the second letter is a weak letter or a hamza
            // then remove it
            if (word.charAt(1) == 'و' || word.charAt(1) == 'ي' || word.charAt(1) == 'ا' || word.charAt(1) == 'ئ') {
                root = word.substring(0, 1);
                root = root + word.substring(2);

                root = middleWeak(root);
                if (rootFound) {
                    return root;
                }
            }

            // if the second letter has a hamza, and it's not on a alif
            // then it must be returned to the alif
            if (word.charAt(1) == 'ؤ' || word.charAt(1) == 'ئ') {
                if (word.charAt(2) == 'م' || word.charAt(2) == 'ز' || word.charAt(2) == 'ر') {
                    root = word.substring(0, 1);
                    root = root + 'ا';
                    root = root + word.substring(2);
                } else {
                    root = word.substring(0, 1);
                    root = root + 'أ';
                    root = root + word.substring(2);
                }
            }

            // if the last letter is a shadda, remove it and
            // duplicate the last letter
            if (word.charAt(2) == 'ّ') {
                root = word.substring(0, 1);
                root = root + word.substring(1, 2);
            }
        }

        // if word is a root, then rootFound is true
        if (root.length() == 0) {
            if (((ArrayList) STAT_FILES.get(16)).contains(word)) {
                rootFound = true;
                if (rootNotFound) {
                    for (int i = 0; i < number; i++) {
                        wordsNotStemmed.remove(wordsNotStemmed.size() - 1);
                    }
                    rootNotFound = false;
                }
                return word;
            }
        } // check for the root that we just derived
        else if (((ArrayList) STAT_FILES.get(16)).contains(root)) {
            rootFound = true;
            if (rootNotFound) {
                for (int i = 0; i < number; i++) {
                    wordsNotStemmed.remove(wordsNotStemmed.size() - 1);
                }
                rootNotFound = false;
            }
            return root;
        }

        if (root.length() == 3) {
            possibleRoots[number][1] = root;
            number++;
        } else {
            possibleRoots[number][1] = word;
            number++;
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // if the word has four letters
    private void isFourLetters(String word) {
        // if word is a root, then rootFound is true
        if (((ArrayList) STAT_FILES.get(12)).contains(word)) {
            rootFound = true;
        }
    }

    //--------------------------------------------------------------------------
    // check if the word matches any of the patterns
    private String checkPatterns(String word) {
        StringBuilder root = new StringBuilder("");
        // if the first letter is a hamza, change it to an alif
        if (word.length() > 0) {
            if (word.charAt(0) == 'أ' || word.charAt(0) == 'إ' || word.charAt(0) == 'آ') {
                root.append("j");
                root.setCharAt(0, 'ا');
                root.append(word.substring(1));
                word = root.toString();
            }
        }

        // try and find a pattern that matches the word
        ArrayList patterns = (ArrayList) STAT_FILES.get(15);
        int numberSameLetters = 0;
        String pattern = "";
        String modifiedWord = "";

        // for every pattern
        for (int i = 0; i < patterns.size(); i++) {
            pattern = (String) patterns.get(i);
            root.setLength(0);
            // if the length of the words are the same
            if (pattern.length() == word.length()) {
                numberSameLetters = 0;
                // find out how many letters are the same at the same index
                // so long as they're not a fa, ain, or lam
                for (int j = 0; j < word.length(); j++) {
                    if (pattern.charAt(j) == word.charAt(j)
                            && pattern.charAt(j) != 'ف'
                            && pattern.charAt(j) != 'ع'
                            && pattern.charAt(j) != 'ل') {
                        numberSameLetters++;
                    }
                }

                // test to see if the word matches the pattern �����
                if (word.length() == 6 && word.charAt(3) == word.charAt(5) && numberSameLetters == 2) {
                    root.append(word.charAt(1));
                    root.append(word.charAt(2));
                    root.append(word.charAt(3));
                    modifiedWord = root.toString();
                    modifiedWord = isThreeLetters(modifiedWord);
                    if (rootFound) {
                        return modifiedWord;
                    } else {
                        root.setLength(0);
                    }
                }

                // if the word matches the pattern, get the root
                if (word.length() - 3 <= numberSameLetters) {
                    // derive the root from the word by matching it with the pattern
                    for (int j = 0; j < word.length(); j++) {
                        if (pattern.charAt(j) == 'ف'
                                || pattern.charAt(j) == 'ع'
                                || pattern.charAt(j) == 'ل') {
                            root.append(word.charAt(j));
                        }
                    }

                    modifiedWord = root.toString();
                    modifiedWord = isThreeLetters(modifiedWord);

                    if (rootFound) {
                        word = modifiedWord;
                        return word;
                    }
                }
            }
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // handle duplicate letters in the word
    private String duplicate(String word) {
        // check if a letter was duplicated
        if (((ArrayList) STAT_FILES.get(1)).contains(word)) {
            // if so, then return the deleted duplicate letter
            word = word + word.substring(1);
            rootFound = true;
            return word;
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // check if the last letter of the word is a weak letter
    private String lastWeak(String word) {
        StringBuilder stemmedWord = new StringBuilder("");
        // check if the last letter was an alif
        if (((ArrayList) STAT_FILES.get(4)).contains(word)) {
            stemmedWord.append(word);
            stemmedWord.append("ا");
            word = stemmedWord.toString();
            rootFound = true;
            return word;
        } // check if the last letter was an hamza
        else if (((ArrayList) STAT_FILES.get(5)).contains(word)) {
            stemmedWord.append(word);
            stemmedWord.append("أ");
            word = stemmedWord.toString();
            rootFound = true;
            return word;
        } // check if the last letter was an maksoura
        else if (((ArrayList) STAT_FILES.get(6)).contains(word)) {
            stemmedWord.append(word);
            stemmedWord.append("ى");
            word = stemmedWord.toString();
            rootFound = true;
            return word;
        } // check if the last letter was an yah
        else if (((ArrayList) STAT_FILES.get(7)).contains(word)) {
            stemmedWord.append(word);
            stemmedWord.append("ي");
            word = stemmedWord.toString();
            rootFound = true;
            return word;
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // check if the first letter is a weak letter
    private String firstWeak(String word) {
        StringBuilder stemmedWord = new StringBuilder("");
        // check if the firs letter was a waw
        if (((ArrayList) STAT_FILES.get(2)).contains(word)) {
            stemmedWord.append("و");
            stemmedWord.append(word);
            word = stemmedWord.toString();
            rootFound = true;
            return word;
        } // check if the first letter was a yah
        else if (((ArrayList) STAT_FILES.get(3)).contains(word)) {
            stemmedWord.append("ي");
            stemmedWord.append(word);
            word = stemmedWord.toString();
            rootFound = true;
            return word;
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // check if the middle letter of the root is weak
    private String middleWeak(String word) {
        StringBuilder stemmedWord = new StringBuilder("j");
        // check if the middle letter is a waw
        if (((ArrayList) STAT_FILES.get(8)).contains(word)) {
            // return the waw to the word
            stemmedWord.setCharAt(0, word.charAt(0));
            stemmedWord.append("و");
            stemmedWord.append(word.substring(1));
            word = stemmedWord.toString();
            // root was found, so set variable
            rootFound = true;
            return word;
        } // check if the middle letter is a yah
        else if (((ArrayList) STAT_FILES.get(9)).contains(word)) {
            // return the waw to the word
            stemmedWord.setCharAt(0, word.charAt(0));
            stemmedWord.append("ي");
            stemmedWord.append(word.substring(1));
            word = stemmedWord.toString();
            // root was found, so set variable
            rootFound = true;
            return word;
        }
        return word;
    }

    //--------------------------------------------------------------------------
    // remove diacritics from the word
    private String removeDiacritics(String currentWord) {
        StringBuilder sb = new StringBuilder();
        ArrayList diacritics = (ArrayList) STAT_FILES.get(17);

        for (char c : currentWord.toCharArray()) // if the character is not a diacritic, append it to modified word
        {
            if (!(diacritics.contains(c))) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    //--------------------------------------------------------------------------
    // check that the word is a stopword
    private boolean checkStopwords(String currentWord) {
        ArrayList v = (ArrayList) STAT_FILES.get(13);
        if (stopwordFound = v.contains(currentWord)) {

        }
        return stopwordFound;
    }

    @Override
    public String stem(String s) {
        String formattedWord = formatWord(s);
        if(formattedWord !=null){
        return stemWord(formattedWord);
        }else{
            return null;
        }
    }

    private String formatWord(String currentWord) {

        // remove any diacritics (short vowels)
        currentWord = removeDiacritics(currentWord);

        // check for stopwords
        if (!checkStrangeWords(currentWord)) // check for stopwords
        {
            if (!checkStopwords(currentWord)) {
                            return currentWord;
            }else{
                return null;
            }
        }else{
            return currentWord;
        }
    }

    private boolean checkStrangeWords(String currentWord) {
        List<String> v = (List<String>) STAT_FILES.get(18);
        return v.contains(currentWord);

    }
}
