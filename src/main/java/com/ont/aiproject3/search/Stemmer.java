/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.search;

/**
 *
 * @author tareq
 * @param <T>
 */
public interface Stemmer<T> {
    
    public T stem(String s);
    
}
