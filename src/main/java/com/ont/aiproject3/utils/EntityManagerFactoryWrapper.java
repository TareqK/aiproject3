/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ont.aiproject3.utils;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author tareq
 */
public class EntityManagerFactoryWrapper {

    private static final Logger LOG = Logger.getLogger(EntityManagerFactoryWrapper.class.getName());

    private static EntityManagerFactory EMF;

    public static EntityManager getEntityManager() {
        return EMF.createEntityManager();
    }

    public static void destroy() {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            DriverManager.getConnection("jdbc:derby:files_db;shutdown=true");
        } catch (ClassNotFoundException | SQLException ex) {
            LOG.severe(ex.getMessage());
        }
        EMF.close();
    }

    public static void create(String pu) {

        EMF = Persistence.createEntityManagerFactory(pu);
    }
}
