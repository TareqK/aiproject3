$(document).ready(main);

function main() {
    $("#searchButton").click(search);
    $("#uploadButton").click(upload);
    $("#form").keydown(function (e) {
        if (e.which !== 13) {
            return;
        } else {
            e.preventDefault();
            this.blur();
            this.submit();
            search();
        }
    });
}

function search() {
    var query = $("#query").val();
    var postData = {"query": query};

    if (query === '') {
        $('input[type="text"],input[type="password"]').css("border", "2px solid red");
        $('input[type="text"],input[type="password"]').css("box-shadow", "0 0 3px red");
        alert("Please fill all fields...!!!!!!");
    } else {
        $.ajax({
            url: "/search/v1/query",
            type: "POST",
            data: JSON.stringify(postData),
            contentType: "application/json",
            dataType: "json",
            success: querySuccess,
            error: queryError
        });
    }
}

function upload() {
    var ele = document.getElementById($("#fileSelector").attr("id"));
    console.log(ele.files);
    var files = ele.files;
    var length = files.length;
    var i = 0;
    html = "remaining : " + length;
    $("#remaining").html(html);
    for (i = 0; i < files.length; i++) {
        var data = new FormData();
        data.append("file", files[i]);
        $.ajax({
            url: "/search/v1/file",
            type: "POST",
            data: data,
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            success: function () {
                length = length - 1;
                html = "remaining : " + length;
                $("#remaining").html(html);
            },
            error: function (e) {
                console.log(e);
            }
        });
    }


}



function querySuccess(data) {
    html = "<ul>";

    var i;
    for (i = 0; i < data.length; i++) {
        html = html + "<li>" + "<a href=\"v1/file/" + data[i].fileId + "\">" + data[i].canonicalName + "</a></li>";
    }
    html = html + "</ul>";
    $("#results").html(html);

}

function queryError(jqHXR, textStatus, errorThrown) {
    if (jqHXR.status === 401 || jqHXR.status === 403) {
        $('input[type="text"],input[type="password"]').css("border", "2px solid red");
        $('input[type="text"],input[type="password"]').css("box-shadow", "0 0 3px red");
        alert("Username or password wrong!!!");
    } else {
        html = "<ul></ul>";
        $("#results").html(html);
        alert("لا يوجد نتيجة بخث");
    }
}
