/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(main);

function main() {
    $("#loginButton").click(login);
}

function login() {
    var userName = $("#username").val();
    var password = $("#password").val();
    var postData = {"username": userName, "password": password};
    if (userName === '' || password === '') {
        $('input[type="text"],input[type="password"]').css("border", "2px solid red");
        $('input[type="text"],input[type="password"]').css("box-shadow", "0 0 3px red");
        alert("Please fill all fields...!!!!!!");
    } else {
        $.ajax({
            url: "/api/v1/auth",
            type: "POST",
            data: JSON.stringify(postData),
            contentType: "application/json",
            dataType: "json",
            success: loginSuccess,
            error: loginError
        });
    }
}

function loginSuccess(data) {
    $(location).attr('href', '/api/app.html');

}

function loginError(jqHXR, textStatus, errorThrown) {
    if (jqHXR.status === 401 || jqHXR.status === 403) {
        $('input[type="text"],input[type="password"]').css("border", "2px solid red");
        $('input[type="text"],input[type="password"]').css("box-shadow", "0 0 3px red");
        alert("Username or password wrong!!!");
    } else {
        alert("Something wrong seems to have happend. Please contact your system administrator");
    }
}