/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var source;
$(document).ready(main);
var eventStream = window.location.protocol  + "/api/event";
function main() {
    source = new EventSource(eventStream, {withCredentials: true});

    source.addEventListener('message', function (e) {
         $("#content").html(e.data);
    }, false);

    source.addEventListener('open', function (e) {
        $("#content").html(e);
    }, false);

    source.addEventListener('error', function (e) {
         $("#content").html(e);
        if (e.readyState == EventSource.CLOSED) {
            // Connection was closed.
        }
    }, false);
    
    source.addEventListener('ping',function(e){
        console.log(e.data);
    },false);
    
    source.addEventListener('test', function (e) {
        $("#content").html(e.data);
    }, false);
}